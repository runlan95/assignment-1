<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
      try{
            $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8',
                           DB_USER, DB_PWD,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
         }catch(PDOException $e){
           echo $e->getMessage();
         }
		}
    }
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
      try{
		  $booklist = array();
      $sql = 'SELECT * FROM Book';

      foreach($this->db->query($sql) as $row) {
        $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
      }
        return $booklist;
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */

    public function getBookById($id)
    {
      try{
      if(is_numeric($id) != false && $id > -1){
      $stmt = $this->db->prepare("SELECT * FROM Book WHERE id=?");
      $stmt->execute(array($id));
      $info = $stmt->fetch(PDO::FETCH_ASSOC);
      if($info != ""){
        $book = new Book($info['title'], $info['author'], $info['description'], $info['id'] );
        return $book;
          }
        }
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
      try{
      if($book->title != '' && $book->title != null){
        if($book->author != '' && $book->author != null){
      $stmt = $this->db->prepare("INSERT INTO Book (title, author, description) VALUES (?, ?, ?)");
      $stmt->execute(array($book->title, $book->author, $book->description));
      $book->id = $this->db->lastInsertId();
    }
    }
  }catch (PDOException $e){
    echo $e->getMessage();
  }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
      try{
      if(is_numeric($book->id) != false && $book->id > -1){
      if($book->title != ''){
        if($book->author != ''){
      $stmt = $this->db->prepare("UPDATE Book SET title=?, author=?, description=? WHERE id=?");
      $stmt->bindValue(1, $book->title, PDO::PARAM_STR);
      $stmt->bindValue(2, $book->author, PDO::PARAM_STR);
      $stmt->bindValue(3, $book->description, PDO::PARAM_STR);
      $stmt->bindValue(4, $book->id, PDO::PARAM_INT);
      $stmt->execute();
          }
        }
      }
    }catch(PDOException $e){
      echo $e->getMessage();
    }
  }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
      try{
      if(is_numeric($id) != false && $id > -1){
      $stmt = $this->db->prepare("DELETE FROM Book WHERE id=?");
      $stmt->bindValue(1, $id, PDO::PARAM_INT);
      $stmt->execute();
        }
      }catch(PDOException $e){
        echo $e->getMessage();
      }
    }

}

?>
